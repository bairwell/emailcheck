<?php
namespace Bairwell\EmailCheck;

# php /LinuxFiles/projects/bairwell-emailcheck/emailcheck.php -i ~/Downloads/examples.txt -o ~/output.txt -c 1 -d 30
/**
 * @var array Holds default file locations
 */
$files = array(
    'psl' => __DIR__ . '/publicsuffixlist.txt',
    'spammers' => __DIR__ . '/spammerdomains.txt',
    'common' => __DIR__ . '/commondomains.txt'
);
$main = new Checker();
if (true === isset($_SERVER['argc'])) {
    $cli = new CLI($main, $files);
    $cli->run();
} else {
    print 'This is a command line PHP script.';
    exit(0);
}


/**
 * Provides a CLI interface to the email check system
 */
class CLI
{

    /**
     * @var Checker The main checker class
     */
    private $checker;

    /**
     * @var array List of default filenames
     */
    private $files;

    /**
     * @param Checker $checker  The main checker class
     * @param array $files Our default list of files
     */
    public function __construct(Checker $checker, $files)
    {
        $this->checker = $checker;
        $this->files = $files;
    }

    /**
     * @param array $options Any options we want to set by default
     * @throws \Exception If any settings are invalid
     */
    public function run($options = array())
    {
        $clioptions = getopt(
            'hi:o:s:c:d:',
            array(
                'input',
                'output',
                'stats',
                'help',
                'nopsl',
                'nospammers',
                'nocommon',
                'nodns',
                'checkpoint',
                'domaincount',
                'psldomains',
                'spammersdomains',
                'commondomains'
            )
        );
        if (false === is_array($clioptions)) {
            throw new \Exception('There was a problem reading in the CLI options.');
        }
        $options = array_merge($options, $clioptions);
        $settings = array(
            'input' => $this->mergeOptions($options, 'i', 'input'),
            'checkpoint' => $this->mergeOptions($options, 'c', 'checkpoint'),
            'usedns' => true,
        );
        if (true === isset($options['nodns'])) {
            $settings['usedns'] = false;
        }
        /**
         * Now set our data files
         */
        $psl = new PublicSuffixList();
        $msg = '';
        if (false === isset($options['nopsl'])) {
            if (true === isset($options['psldomains'])) {
                $msg = $psl->setFilename($options['psldomains']);
                if (is_string($msg)) {
                    print $msg . ' : Disabling PublicSuffixList' . PHP_EOL;
                }
            } else {
                $msg = $psl->setFilename($this->files['psl']);
                if (is_string($msg)) {
                    print $msg . ' : Disabling PublicSuffixList' . PHP_EOL;
                }
            }

        }
        if (false === is_string($msg)) {
            $psl->parse();
            $this->checker->setPslDomains($psl);
        }
        $domains = new DomainList();
        $msg = '';
        if (false === isset($options['nocommon'])) {
            if (true === isset($options['commondomains'])) {
                $msg = $domains->setFilename($options['commondomains']);
                if (is_string($msg)) {
                    print $msg . ' : Disabling CommonDomains' . PHP_EOL;
                }
            } else {
                $msg = $domains->setFilename($this->files['common']);
                if (is_string($msg)) {
                    print $msg . ' : Disabling CommonDomains' . PHP_EOL;
                }
            }

        }
        if (false === is_string($msg)) {
            $domains->parse();
            $this->checker->setCommonDomains($domains);
        }
        $domains = new DomainList();
        $msg = '';
        if (false === isset($options['nospammer'])) {
            if (true === isset($options['spammerdomains'])) {
                $msg = $domains->setFilename($options['spammerdomains']);
                if (is_string($msg)) {
                    print $msg . ' : Disabling SpammerDomains' . PHP_EOL;
                }
            } else {
                $msg = $domains->setFilename($this->files['spammers']);
                if (is_string($msg)) {
                    print $msg . ' : Disabling SpammerDomains' . PHP_EOL;
                }
            }

        }
        if (false === is_string($msg)) {
            $domains->parse();
            $this->checker->setSpammerDomains($domains);
        }
        /**
         * Now check CLI specific settings
         */

        $output = $this->mergeOptions($options, 'o', 'output');
        $stats = $this->mergeOptions($options, 's', 'stats');
        $help = $this->mergeOptions($options, 'h', 'help');
        $domaincount = $this->mergeOptions($options, 'd', 'domaincount');
        if (null !== $help) {
            $this->displayHelp();
            return;
        }
        if (false === is_string($settings['input']) || strlen($settings['input']) < 1) {
            print 'No input filename specified' . PHP_EOL;
            $this->displayHelp();
            return;
        }
        $outputcheck = false;
        if (true === is_string($output) && strlen($output) > 1) {
            $outputcheck = $this->checkOutputFile($output, 'output file');
        }
        $statscheck = false;
        if (true === is_string($stats) && strlen($stats) > 1) {
            $statscheck = $this->checkOutputFile($stats, 'stats file');
        }
        if (true === is_string($domaincount) && strlen($domaincount) > 0) {
            $domaincount = preg_replace('/[^0-9]/', '', $domaincount);
            $domaincount = intval($domaincount, 10);
        }
        if (false === is_int($domaincount) || $domaincount < 10) {
            $domaincount = 10;
        }
        if (true === $outputcheck || true === $statscheck) {
            print 'Are you sure you wish to continue? (y/N)?' . PHP_EOL;
            $in = fopen("php://stdin", "r");
            $in_string = fgets($in, 10);
            fclose($in);
            if ('Y' !== mb_substr(mb_strtoupper(trim($in_string)), 0, 1)) {
                return;
            } else {
                print 'continuing...' . PHP_EOL;
            }
        }
        /**
         * And let's get ready!
         */
        $this->checker->set($settings);
        $this->checker->setCheckpointCallback(array($this, 'checkpointCallback'));
        print str_repeat('=', 30) . PHP_EOL;
        if (true === is_string($stats)) {
            print 'Stats will be sent to ' . $stats . ' on completion' . PHP_EOL;
        }
        if (true === is_string($output)) {
            print 'Emails will be written to ' . $output . ' on completion' . PHP_EOL;
        }
        $this->checker->showSettings();
        print str_repeat('=', 30) . PHP_EOL;
        print 'Starting in 5 seconds time....';
        sleep(5);
        print PHP_EOL;
        $start_time = time();
        $this->checker->run();
        /**
         * Let's prepare the stats
         */
        $stats_text = 'Started at ' . date('Y-m-d H:i:s', $start_time) .
            ' Ended at ' . date('Y-m-d H:i:s', time()) . PHP_EOL;


        $stats_text .= $this->statsText();
        $sliced = array_slice($this->checker->getDomaincounter(), 0, $domaincount, true);
        $pos = 1;
        $invalidDomains=$this->checker->getFailsDNS();
        foreach ($sliced as $key => $value) {
            $stats_text .= $pos . '. ' . $key . ' (' . number_format($value) . ' emails) ';
            $isCommon = false;
            if (null !== $this->checker->getCommonDomains()) {
                if ($this->checker->getCommonDomains()->check($key)) {
                    $stats_text .= '[Common domain] ';
                    $isCommon = true;
                }
            }
            if (false === $isCommon) {
                /**
                 * If it is a common domain, it isn't checked against Spammers and invalid domains
                 */
                if (null !== $this->checker->getSpammerDomains()) {
                    if ($this->checker->getSpammerDomains()->check($key)) {
                        $stats_text .= '[Spammer domain] ';
                    }
                }
                if (true===isset($invalidDomains[$key])) {
                    $stats_text .= '[Invalid domain] ';
                }
            }
            $stats_text .= PHP_EOL;
            $pos++;
        }
        if (true === is_string($stats) && strlen($stats) > 1) {
            $outhandle = fopen($stats, 'wt');
            if (false === $outhandle) {
                print 'Stats file ' . $stats . ' could not be opened' . PHP_EOL;
                exit(0);
            }
            fwrite($outhandle, $stats_text);

            fclose($outhandle);
        } else {
            print $stats_text;
        }
        /**
         * And now the output
         */
        $emails = array_merge($this->checker->getEmails(), $this->checker->getCommonEmails());
        $spammers = $this->checker->getSpammerEmails();
        if (true === is_string($output) && strlen($output) > 1) {
            $outhandle = fopen($output, 'wt');
            if (false === $outhandle) {
                throw new \Exception('Output file ' . $output . ' could not be opened' . PHP_EOL);
            }
            fwrite($outhandle, 'Good emails' . PHP_EOL);
            fwrite($outhandle, '===========' . PHP_EOL);
            foreach ($emails as $key => $value) {
                fwrite($outhandle, $value . PHP_EOL);
            }
            fwrite($outhandle, PHP_EOL . PHP_EOL . PHP_EOL . 'Possibly spammer emails' . PHP_EOL);
            fwrite($outhandle, '=======================' . PHP_EOL);
            foreach ($spammers as $key => $value) {
                fwrite($outhandle, $value . PHP_EOL);
            }
            fclose($outhandle);
        } else {
            print 'Good emails' . PHP_EOL;
            print '===========' . PHP_EOL;
            foreach ($emails as $key => $value) {
                print $value . PHP_EOL;
            }
            print 'Possibly spammer emails' . PHP_EOL;
            print '=======================' . PHP_EOL;
            foreach ($spammers as $key => $value) {
                print $value . PHP_EOL;
            }
        }
    }

    /**
     * Display the help text
     */
    private function displayHelp()
    {
        print 'This script scans a text file for email addresses and lists unique email addresses reduced to "common sections".' . PHP_EOL;
        print 'It will not work with email addresses that are split across multiple lines.' . PHP_EOL;
        print 'Parameters are:' . PHP_EOL;
        print ' --input filename (or -i filename)  : The input filename' . PHP_EOL;
        print ' --output filename (or -o filename) : The optional output filename' . PHP_EOL;
        print ' --stats filename (or -s filename)  : The optional stats filename' . PHP_EOL;
        if (true === isset($this->files['psl'])) {
            print ' --psldomains filename              : The optional PSL filename (defaults to ' . $this->files['psl'] . ' if exists)' . PHP_EOL;
            print ' --nopsl                            : Disables checks against Mozilla\'s public service list (PSL)' . PHP_EOL;
        }
        if (true === isset($this->files['spammers'])) {
            print ' --spammersdomains filename         : The optional list of spamming domains (defaults to ' . $this->files['spammers'] . ' if exists)' . PHP_EOL;
            print ' --nospammers                       : Disables checks against the spammers domains list' . PHP_EOL;
        }
        if (true === isset($this->files['common'])) {
            print ' --commondomains filename           : The optional list of common domains (defaults to ' . $this->files['common'] . ' if exists)' . PHP_EOL;
            print ' --nocommon                         : Disables checks against the common domains list' . PHP_EOL;
        }
        print ' --checkpoint number (or -c number) : Report a checkpoint every number percent (default 10%: range is 0.01% to 50%)' . PHP_EOL;
        print ' --domaincount number (or -d number): Number of domains to list in stats (default 10: range is 10 upwards)' . PHP_EOL;

        print ' --nodns                            : Disables domain name lookups of MX records' . PHP_EOL;

        print 'If an output file is not specified, then the list of emails will be output to the screen.' . PHP_EOL;
        print 'If an stats file is not specified, the stats will be output to the screen.' . PHP_EOL;
    }

    /**
     * Merges a short and long settings from the options array. Long settings take precedence
     * @param array $array Our options array
     * @param string $short The label for any short setting (such as "-i")
     * @param string $long The label for any long setting (such as "--input")
     * @return string Any set details
     */
    private function mergeOptions($array, $short, $long)
    {
        $out = null;
        if (true === isset($array[$short])) {
            $out = $array[$short];
        }
        if (true === isset($array[$long])) {
            $out = $array[$long];
        }
        return $out;
    }


    /**
     * @param $file
     * @param $prompt
     * @return bool
     * @throws \Exception
     */
    private function checkOutputFile($file, $prompt)
    {
        $state = false;
        if (true === file_exists($file)) {
            print $file . ' already exists for ' . $prompt . PHP_EOL;
            $state = true;
            if (true !== is_writable($file)) {
                throw new \Exception($file . ' cannot be overwritten or modified for ' . $prompt);

            }
        }
        return $state;

    }

    /**
     * @param $data
     */
    public function checkpointCallback($data)
    {
        $time_per_percent = $data['timeran'] / $data['percentage'];
        $eta = round($time_per_percent * (100 - $data['percentage']));
        print PHP_EOL . 'Completed ' . $data['percentage'] . '%' . PHP_EOL;
        print 'Rate: 1% every ' . round($time_per_percent) . ' seconds' . PHP_EOL;
        print 'Estimated completion in ';
        print gmdate('H\h\r i\m\i\n s\s\e\c ', $eta);
        print 'at ' . date('Y-m-d H:i:s', strtotime('+' . $eta . ' seconds', time())) . PHP_EOL;
        print $this->statsText();

    }

    /**
     * @return string
     */
    private function statsText()
    {
        $common = count($this->checker->getCommonEmails());
        $spammers = count($this->checker->getSpammerEmails());
        $statstext = 'Number of lines: ' . number_format($this->checker->getLinesExtracted()) . ' lines.' . PHP_EOL;
        $statstext .= 'Items which looked like email addresses: ' .number_format($this->checker->getLooksLikeEmails()) . PHP_EOL;
        if (null !== $this->checker->getSpammerDomains()) {
            $statstext .= 'Email addresses which are flagged as spammers: ' .
                number_format($spammers) . PHP_EOL;
        }
        if (null !== $this->checker->getCommonDomains()) {
            $statstext .= 'Email addresses which are flagged as on common domains: ' .
                number_format($common) . PHP_EOL;
        }
        if (null !== $this->checker->getPslDomains()) {
            $statstext .= 'Email addresses which failed PSL lookups: ' .
                number_format($this->checker->getFailsPSL()) .
                ' (passed ' .
                number_format($this->checker->getPassesPSL()) . ')' . PHP_EOL;
        }
        if (true === $this->checker->getUseDNS()) {
            $statstext .= 'Email addresses which failed DNS lookups: ' .
                number_format(count($this->checker->getFailsDNS())) .
                ' (passed ' .
                number_format(count($this->checker->getPassesDNS())) . ')' . PHP_EOL;
        }
        $statstext .= 'Email addresses not in above: ' . number_format(count($this->checker->getEmails())) . PHP_EOL;
        $statstext .= 'Found ' .
            number_format(count($this->checker->getEmails()) + $common + $spammers) .
            ' total email addresses' . PHP_EOL;
        $statstext .= 'Duplicated email addresses: ' . number_format($this->checker->getDuplicatedEmail()) . PHP_EOL;
        return $statstext;
    }
}

/**
 *
 */
class Checker
{

    /**
     * @var string Input filename
     */
    private $input;

    /**
     * @var float The percentage checkpoint
     */
    private $checkpoint;

    /**
     * @var boolean Are we using DNS lookups?
     */

    private $useDNS;
    /**
     * @var DomainList What PSL data are we using?
     */
    private $pslDomains;

    /**
     * @var DomainList What spammer domains list data are we using?
     */
    private $spammerDomains;

    /**
     * @var DomainList What common domains list data are we using?
     */
    private $commonDomains;

    /**
     * @var callback What callback do we use for checkpoints?
     */
    private $checkpointCallback;

    /**
     * @var callback What callback do we use for verbose information?
     */
    private $verboseCallback;

    /**
     * @var array Extracted email address
     */
    private $emails;

    /**
     * @var array Number of email addresses on the domains
     */
    private $domaincounter;

    /**
     * @var int The number of lines we've processed
     */
    private $linesExtracted;

    /**
     * @var int Number of items that look like email addresses
     */
    private $looksLikeEmails;

    /**
     * @var
     */
    private $passesPSL;

    /**
     * @var
     */
    private $failsPSL;

    /**
     * @var
     */
    private $duplicatedEmail;

    /**
     * @var
     */
    private $passesDNS;

    /**
     * @var
     */
    private $failsDNS;

    /**
     * @var array Extracted spammer email addresses
     */
    private $spammerEmails;

    /**
     * @var array Extracted common email addresses
     */
    private $commonEmails;

    /**
     *
     */
    public function __construct()
    {
        $this->input = null;
        $this->checkpoint = 10;
        $this->useDNS = true;
        $this->commonDomains = null;
        $this->pslDomains = null;
        $this->spammerDomains = null;
        $this->checkpointCallback = null;
        $this->verboseCallback = null;
    }

    /**
     *
     */
    public function reset()
    {
        $this->linesExtracted = 0;
        $this->looksLikeEmails = 0;
        $this->passesPSL = 0;
        $this->failsPSL = 0;
        $this->duplicatedEmail = 0;
        $this->passesDNS = array();
        $this->failsDNS = array();
        $this->spammerEmails = array();
        $this->commonEmails = array();
        $this->emails = array();
    }

    /**
     * @param $settings
     */
    public function set($settings)
    {
        if (true === isset($settings['input'])) {
            $this->setInput($settings['input']);
        }
        if (true === isset($settings['checkpoint'])) {
            $this->setCheckpoint($settings['checkpoint']);
        }
        if (true === isset($settings['usedns'])) {
            $this->setUseDNS($settings['usedns']);
        }
    }

    /**
     * Show our settings
     */
    public function showSettings()
    {
        print 'Checkpoints will be printed every ' . $this->checkpoint . '%' . PHP_EOL;
        if (null === $this->commonDomains) {
            print 'Ignoring Common Domains checks' . PHP_EOL;
        } else {
            print 'Using Common Domain list from ' . $this->commonDomains->getFilename() . PHP_EOL;
        }
        if (null === $this->spammerDomains) {
            print 'Ignoring Spammer Domains checks' . PHP_EOL;
        } else {
            print 'Using Spammers Domain list from ' . $this->spammerDomains->getFilename() . PHP_EOL;
        }
        if (null === $this->pslDomains) {
            print 'Ignoring Public Suffix List checks' . PHP_EOL;
        } else {
            print 'Using Public Suffix List from ' . $this->pslDomains->getFilename() . PHP_EOL;
        }
        if (false === $this->useDNS) {
            print 'Ignoring DNS checks' . PHP_EOL;
        } else {
            print 'Performing DNS checks' . PHP_EOL;
        }
    }

    /**
     * @throws \Exception
     */
    public function run()
    {
        if (null === $this->input) {
            throw new \Exception('No input file');
        }
        /**
         * Open the file
         */
        $filesize = filesize($this->input);
        $handle = fopen($this->input, 'rt');
        if (false === $handle) {
            throw new \Exception($this->input . ' could not be opened');
        }
        $maxlength = 2048;
        /**
         * Start reading
         */
        $line = fgets($handle, $maxlength);
        $this->reset();
        $nextcheckpoint = $this->checkpoint;
        $time_start = microtime(true);

        while (false !== $line) {
            $this->linesExtracted++;
            $pos = ftell($handle);
            if (($pos / $filesize) * 100 > $nextcheckpoint) {
                $percentage = (round((($pos / $filesize) * 100), 2));
                $timeran = (microtime(true) - $time_start);
                if (true === is_callable($this->checkpointCallback)) {
                    call_user_func(
                        $this->checkpointCallback,
                        array('percentage' => $percentage, 'timeran' => $timeran)
                    );
                }
                $nextcheckpoint = (($pos / $filesize) * 100) + $this->checkpoint;
            }
            /**
             * See if the line contains one or more email address
             */
            $matches = array();
            $search = preg_match_all(
                '/(?:[a-z0-9!#$%&\'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+\/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/',
                $line,
                $matches,
                PREG_PATTERN_ORDER
            );
            if (false === $search) {
                fclose($handle);
                throw new \Exception('Regular expression match failed');
            }
            if (0 < $search) {
                /**
                 * Now lets see which of these "likely email addresses" are valid
                 */
                foreach ($matches as $email) {
                    $this->looksLikeEmails++;
                    $email = mb_strtolower($email[0], 'UTF-8');
                    $atposition = mb_strrpos($email, '@');
                    $address = mb_substr($email, 0, $atposition);
                    $domain = mb_substr($email, $atposition + 1);
                    $continue = true;
                    /**
                     * Check for "known equal domains"
                     */
                    if (1 === preg_match('/^(gmail.com|googlemail.com|gmail.de)$/', $domain)) {
                        /**
                         * Google accounts do not have full stops in them
                         */
                        if (0 < mb_strpos($address, '.')) {
                            $address = str_replace('.', '', $address);
                        }
                        /**
                         * Standardise google domains
                         */
                        $domain = 'gmail.com';
                    }
                    /**
                     * Setup the domain if needed
                     */
                    if (false === isset($this->domaincounter[$domain])) {
                        $this->domaincounter[$domain] = 0;
                    }
                    $this->domaincounter[$domain]++;
                    /**
                     * Email addresses with + signs on are the same
                     */
                    if (0 < mb_strpos($address, '+')) {
                        $address = mb_substr($address, 0, mb_strpos($address, '+'));
                    }
                    /**
                     * Check if it is a duplicate
                     */
                    if (true === isset($this->emails[$address . '@' . $domain]) || true === isset($this->commonEmails[$address . '@' . $domain]) || true === isset($this->spammerEmails[$address . '@' . $domain])) {
                        $this->duplicatedEmail++;
                        $continue = false;
                    }
                    /**
                     * Do we need to continue checking this domain?
                     */
                    if (true === $continue) {
                        if (null !== $this->commonDomains) {
                            if (true === $this->commonDomains->check($domain)) {
                                $this->commonEmails[$address . '@' . $domain] = $email;
                                $continue = false;
                            }
                        }
                    }
                    if (true === $continue) {
                        /**
                         * Check the domain against the spammers domain if enabled
                         */
                        if (null !== $this->spammerDomains) {
                            if (true === $this->spammerDomains->check($domain)) {
                                $this->spammerEmails[$address . '@' . $domain] = $email;
                                $continue = false;
                            }
                        }
                    }
                    if (true === $continue) {
                        /**
                         * Check the domain against the public suffix list if enabled
                         */
                        if (null !== $this->pslDomains) {
                            if (true === $this->pslDomains->check($domain)) {
                                $this->passesPSL++;
                            } else {
                                $this->failsPSL++;
                                $continue = 'failed psl';
                            }
                        }
                    }
                    if (true === $continue) {
                        /**
                         * Do DNS checks
                         */
                        if (true === $this->useDNS) {
                            if (false===isset($this->failsDNS[$domain]) && false===isset($this->passesDNS[$domain])) {
                                $result = checkdnsrr($domain . '.', 'MX');
                                if (false===$result) {
                                    $this->failsDNS[$domain]=0;
                                } else {
                                    $this->passesDNS[$domain]=0;
                                }
                            }
                            if (true === isset($this->failsDNS[$domain])) {
                                $continue = 'bad domain';
                                $this->failsDNS[$domain]++;
                            } elseif (true === isset($this->passesDNS[$domain])) {
                                $this->passesDNS[$domain]++;
                            } else {
                                throw new \Exception('Unknown domain state for '.$domain);
                            }
                        }
                    }
                    if (true === $continue) {
                        $this->emails[$address . '@' . $domain] = $email;
                    } else {
                        if (false !== $continue) {
                            if (true === is_callable($this->verboseCallback)) {
                                call_user_func(
                                    $this->verboseCallback,
                                    array('email' => $email, 'reason' => $continue)
                                );
                            }
                        }
                    }
                }
            }
            /**
             * Now to read in the next line if appropriate
             */
            if (true === feof($handle)) {
                $line = false;
            } else {
                $line = fgets($handle, $maxlength);
            }
        }
        /**
         * Close file
         */
        fclose($handle);
        /**
         * Sort the data
         */
        arsort($this->domaincounter, SORT_NUMERIC);

    }

    /**
     * @param float $checkpoint
     */
    public function setCheckpoint($checkpoint)
    {
        if (true === is_string($checkpoint) && strlen($checkpoint) > 0) {
            $this->checkpoint = preg_replace('/[^0-9.\,]/', '', $checkpoint);
            $this->checkpoint = floatval($this->checkpoint);
        }
        if (false === is_float($this->checkpoint) || $this->checkpoint < 0.01 || $this->checkpoint > 50) {
            $this->checkpoint = 10;
        }
        return $this;
    }

    /**
     * @return float
     */
    public function getCheckpoint()
    {
        return $this->checkpoint;
    }

    /**
     * @param string $input
     */
    public function setInput($input)
    {
        $realpath = realpath($input);
        if (false === $realpath) {
            throw new \Exception('Unable to find real path of input file ' . $input);
        }
        if (false === file_exists($input)) {
            throw new \Exception('Input file name ' . $input . ' does not exist');
        }
        if (true !== is_readable($input)) {
            throw new \Exception($input . ' is not readable');
        }
        $this->input = $input;
        return $this;
    }

    /**
     * @return string
     */
    public function getInput()
    {
        return $this->input;
    }

    /**
     * @param boolean $dns
     */
    public function setUseDNS($dns)
    {
        if (false === is_bool($dns)) {
            throw new \Exception('Invalid DNS setting');
        }
        $this->useDNS = $dns;
    }

    /**
     * @return boolean
     */
    public function getUseDNS()
    {
        return $this->useDNS;
    }

    /**
     * @param callable $checkpointCallback
     */
    public function setCheckpointCallback($checkpointCallback)
    {
        if (false === is_callable($checkpointCallback)) {
            throw new \Exception('Invalid checkpoint callback');
        }
        $this->checkpointCallback = $checkpointCallback;
    }

    /**
     * @return callable
     */
    public function getCheckpointCallback()
    {
        return $this->checkpointCallback;
    }

    /**
     * @param callable $verboseCallback
     */
    public function setVerboseCallback($verboseCallback)
    {
        if (false === is_callable($verboseCallback)) {
            throw new \Exception('Invalid verbose callback');
        }
        $this->verboseCallback = $verboseCallback;
    }

    /**
     * @return callable
     */
    public function getVerboseCallback()
    {
        return $this->verboseCallback;
    }

    /**
     * @return array
     */
    public function getDomaincounter()
    {
        return $this->domaincounter;
    }

    /**
     * @return mixed
     */
    public function getDuplicatedEmail()
    {
        return $this->duplicatedEmail;
    }

    /**
     * @return array
     */
    public function getEmails()
    {
        return $this->emails;
    }

    /**
     * @return mixed
     */
    public function getFailsDNS()
    {
        return $this->failsDNS;
    }

    /**
     * @return mixed
     */
    public function getFailsPSL()
    {
        return $this->failsPSL;
    }

    /**
     * @return int
     */
    public function getLinesExtracted()
    {
        return $this->linesExtracted;
    }

    /**
     * @return int
     */
    public function getLooksLikeEmails()
    {
        return $this->looksLikeEmails;
    }

    /**
     * @return mixed
     */
    public function getPassesDNS()
    {
        return $this->passesDNS;
    }

    /**
     * @return mixed
     */
    public function getPassesPSL()
    {
        return $this->passesPSL;
    }

    /**
     * @param \Bairwell\EmailCheck\DomainList $commonDomains
     */
    public function setCommonDomains($commonDomains)
    {
        if (null === $commonDomains || $commonDomains instanceof DomainList) {
            $this->commonDomains = $commonDomains;
        } else {
            throw new \Exception('Invalid common domains setting');
        }
    }

    /**
     * @return \Bairwell\EmailCheck\DomainList
     */
    public function getCommonDomains()
    {
        return $this->commonDomains;
    }

    /**
     * @param \Bairwell\EmailCheck\DomainList $pslDomains
     */
    public function setPslDomains($pslDomains)
    {
        if (null === $pslDomains || $pslDomains instanceof DomainList) {
            $this->pslDomains = $pslDomains;
        } else {
            throw new \Exception('Invalid public suffix list setting');
        }
    }

    /**
     * @return \Bairwell\EmailCheck\DomainList
     */
    public function getPslDomains()
    {
        return $this->pslDomains;
    }

    /**
     * @param \Bairwell\EmailCheck\DomainList $spammerDomains
     */
    public function setSpammerDomains($spammerDomains)
    {
        if (null === $spammerDomains || $spammerDomains instanceof DomainList) {
            $this->spammerDomains = $spammerDomains;
        } else {
            throw new \Exception('Invalid spammer domains setting');
        }
    }

    /**
     * @return \Bairwell\EmailCheck\DomainList
     */
    public function getSpammerDomains()
    {
        return $this->spammerDomains;
    }

    /**
     * @return array
     */
    public function getCommonEmails()
    {
        return $this->commonEmails;
    }

    /**
     * @return array
     */
    public function getSpammerEmails()
    {
        return $this->spammerEmails;
    }


}


/**
 *
 */
class DomainList
{
    /**
     * @var array A list of parsed domains
     */
    protected $rules;

    /**
     * @var string The filename we are using
     */
    protected $filename;

    /**
     *
     */
    public function __construct()
    {
        $this->rules = null;
    }

    /**
     * @throws \Exception
     */
    public function parse()
    {
        $rules = file($this->filename, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
        if (false === is_array($rules)) {
            throw new \Exception($this->filename . ' could not be opened');
        }

        $rules = array_filter(array_map('trim', $rules));
        array_walk(
            $rules,
            function ($v, $k) use (&$rules) {
                if (false !== strpos($v, '//')) {
                    unset($rules[$k]);
                }
            }
        );
        $this->rules = $rules;
    }


    /**
     * Checks to see if a domain appears on the list
     * @param string $domain The domain name
     * @return bool true if it appears, false if not
     */
    public function check($domain)
    {
        if (true === is_array($this->rules)) {
            if (true === in_array($domain, $this->rules)) {
                return true;
            }
            return false;
        } else {
            throw new \Exception('List disabled');
        }
    }

    /**
     * @param string $filename
     */
    public function setFilename($filename)
    {
        $realpath = realpath($filename);
        if (false === $realpath) {
            return 'Unable to find real path of ' . $filename;
        }
        if (false === file_exists($filename)) {
            return 'File name ' . $filename . ' does not exist';
        }
        if (true !== is_readable($filename)) {
            return $filename . ' is not readable';
        }
        $this->filename = $filename;
        return $this;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }


}

/**
 *
 */
class PublicSuffixList extends DomainList
{

    /**
     * @param string $domain
     * @return bool
     * @throws \Exception
     */
    public function check($domain)
    {
        if (true === is_array($this->rules)) {
            $segments = '';
            foreach (array_reverse(explode('.', $domain)) as $s) {
                $wildcard = rtrim('*.' . $segments, '.');
                $segments = rtrim($s . '.' . $segments, '.');

                if (true === in_array('!' . $segments, $this->rules)) {
                    $tld = substr($wildcard, 2);
                    break;
                } elseif (in_array($wildcard, $this->rules) or in_array($segments, $this->rules)) {
                    $tld = $segments;
                }
            }
            if (true === isset($tld)) {
                return true;
            }
            return false;
        } else {
            throw new \Exception('Public Suffix List disabled');
        }
    }

}